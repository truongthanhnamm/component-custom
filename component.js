ko.components.register('mcc-tab', {
  viewModel: function (params) {
    this.dataTab = params.data
    this.dataTab.subscribe(function (newValue) {
      console.log(newValue)
    });
  },
  template: `
  <div class='tab-component' data-bind="foreach: { data: dataTab}">
      <input class="input-tab-component" type="radio" name="tabs" data-bind='attr: { id: "tab" + $index(),checked: $index() === 0}'>
      <label class='lab-tab-component' data-bind='attr: { for: "tab" + $index()}'>
        <i data-bind="class: $data.icon"></i>
        <span data-bind="text: $data.name"></span>
      </label>
      
      <!-- ko if: $index() === ($parent.dataTab().length - 1) -->
        <div class='tab-content' data-bind="foreach: { data: $parent.dataTab}">
          <section data-bind='text: $data.dataContent, attr: { id: "content" + $index()}'></section>
        </div>
      <!-- /ko -->
  </div>
  `
});

ko.components.register('mc-checkbox', {

  viewModel: new function () {
    let me = this;

    this.createViewModel = function (params, componentInfo) {
      me.name = ko.observable(params.name || Math.random().toString(36).substring(6))
      me.checked = ko.observable(params.checked || false)
      me.label = params.label || "Unset label";
      me.onClick = () => {
        me.checked(!me.checked())
      }

      return me;
    }
  },
  template: `
  <input type='checkbox' data-bind='checked: checked, attr: {id: name}'/>
  <label class="check-box cbobox-mr" data-bind='click: onClick, attr: {for: name}'></label>
  <span data-bind='text: label'></span>  
  `
});

ko.components.register('mc-combobox', {
  viewModel: function (params) {
    this.data = params.data || ko.observablearray([]);
    this.defaultText = ko.observable(params.defaultText || "Choose...")
    this.isShow = ko.observable(false);
    this.isSelected = ko.observable(0);
    this.onChangeCallBack = params.onChange || function () {};

    //Open/close
    this.onShow = () => {
      this.isShow(!this.isShow())
    }

    //Change text 
    this.onChange = (index, display) => {
      this.isSelected(index);
      this.defaultText(display)
      this.onChangeCallBack()
    }
  },
  template: `
  <div class="dropdown" data-bind='click: onShow, class: isShow() ? "open" : ""'>
    <span class="current" data-bind='text: defaultText'></span>
    <div class="list">
      <ul class="select" data-bind='foreach: {data: data}'>
        <li class="option " data-bind='text: $data.display,click: ()=>{$parent.onChange($index(),$data.display)}, class: $parent.isSelected() === $index() ? "selected" : ""'>.</li>
      </ul>
    </div>
  </div>
  `
});

ko.components.register('mc-input', {
  viewModel: function (params) {
    this.placeholder = params.placeholder || ""
    this.value = ko.observable(params.value())
    this.type = params.type || "text"
  },
  template: `
  <input data-bind='attr: { placeholder: placeholder,type: type }, value: value'/>
  `
});

ko.components.register('mc-area', {
  viewModel: function (params) {
    this.placeholder = params.placeholder || ""
    this.value = params.value
    this.required = params.required
    this.label = ko.observable(params.label || "Label unset")
    this.name = ko.observable(params.name || Math.random().toString(36).substring(6));
  },
  template: `
  <textarea data-bind='attr: { placeholder: placeholder,id: name}, value: value'></textarea>
  <label data-bind='attr: {for: name}, text: label'></label>
  `
});

ko.components.register('mc-search', {
  viewModel: new function () {
    let me = this;

    this.createViewModel = function (params, componentInfo) {
      me.placeholder = params.placeholder || ""
      me.name = ko.observable(params.name || Math.random().toString(36).substring(6));
      me.length = ko.observable(params.length || 6);
      me.searchAPI = params.searchAPI || function () {};
      let timeout = null;

      let checkSuggsKeywords = function (keywords, data) {
        return keywords == encodeURIComponent(data.toLowerCase())
      }

      let autocomplete = function (inp, arr) {
        console.log(inp);
        console.log(arr);
      }

      let doDelayedSearch = function (val) {
        timeout !== null && clearTimeout(timeout);
        timeout = setTimeout(function () {
          me.searchAPI(val);

          console.log(val);
        }, 500);
      }

      me.onTyping = function (model, e) {
        let {
          value
        } = e.currentTarget
        doDelayedSearch(value)
        return true;
      }
      return me;
    }
  },

  template: `
  <input class="input-search" data-bind='attr: { placeholder: placeholder}, event: {keyup: onTyping}'>
 
  `
}); {
  /* <div id="autocomplete-list" class="autocomplete-items">
      <div>
        <strong>tr</strong>ường giang
        <input type="hidden" value="trường giang">
      </div>
    </div> */
}
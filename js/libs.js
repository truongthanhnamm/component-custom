/**
 * Created by tientm2 on 1/15/2019.
 */
import './Components/Tab.js';
import './Components/Confirm.js';
import './Components/Alert.js';
import './Components/Card.js';
import './Components/Combobox.js';
import './Components/Checkbox.js';
import './Components/Form1.js';
import './Components/Input.js';
import './Components/Search.js';

import './Components/TextArea.js';
// window.mcConfirm.show('Thông báo nè', 'Nội dung thông báo nè', {
//   name: 'Hủy',
//   cb: function () {
//     alert('hàm xử lý btn 1');
//   }
// }, {
//   name: 'Đồng ý',
//   cb: function () {
//     alert('hàm xử lý btn 1');
//   }
// })
class Confirm extends HTMLElement {

  constructor() {
    super();
    this.btnOK = $(`<button class='btn-ok'></button>`);
    this.btnCancel = $(`<button class='btn-cancel'></button>`);
  }
  connectedCallback() {
    this.registryToWindow();
  }

  registryToWindow() {
    window.mcConfirm = this;
  }

  hide() {
    $('.mc-confirm').removeClass('swal2-show').addClass('swal2-hide');
    setTimeout(() => {
      $('.mc-alert-wrapper').remove();
    }, 300);
  }

  geneRateButton(data) {
    this.btnOK.text(data.btnOK.name).click(() => {
      this.hide();
      typeof (data.btnOK.cb) != 'undefined' && data.btnOK.cb();
    });

    data.hasOwnProperty('btnCancel') ? this.btnCancel.text(data.btnCancel.name).click(() => {
      this.hide();
      typeof (data.btnOK.cb) != 'undefined' && data.btnCancel.cb()
    }) : this.btnCancel.addClass('dp-none');
  }

  geneRateComfirm(dataArgument) {
    let data = this.validateAngrument(dataArgument);
    this.geneRateButton(data)
    const wrapper = $(`
    <div class='mc-alert-wrapper'>
      <div class='mc-confirm'>
      <div class='header'>
        <div class='title'>${data.title}</div>
        <button type="button" class="close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class='content'>
        ${data.content}
      </div>
      <div class='button-container'>  
      </div>
    </div>
    </div>`);

    //Thêm 2 button vào wapper
    wrapper.find('.button-container').append(this.btnCancel).append(this.btnOK);

    //Sự kiện click dấu (x) thì ẩn đi
    wrapper.find('button.close').click(() => {
      this.hide();
    })

    return wrapper;
  }

  validateAngrument(data) {
    let realData = {
      title: 'Thông báo',
      content: 'Nội dung chưa được truyền vào, vui lòng kiểm tra lại',
      btnOK: {
        name: 'Đồng ý',
        action: () => {}
      }
    }

    if (data.length && data.length < 5) {
      switch (data.length) {
        case 1:
          realData.content = data[0]
          break;

        case 2:
          // 1 noi dung 1 button
          realData.content = data[0]
          realData.btnOK = {
            ...data[1]
          }
          break;

        case 3:
          if (typeof (data[1]) === 'string') {
            //2string 1button
            realData.title = data[0];
            realData.content = data[1];
            realData.btnOK = {
              ...data[2]
            }
          } else {
            //1string 2button
            realData.content = data[0]
            realData.btnCancel = {
              ...data[1]
            }
            realData.btnOK = {
              ...data[2]
            }

          }
          break;

        case 4:
          realData.title = data[0]
          realData.content = data[1]
          realData.btnCancel = {
            ...data[2]
          }
          realData.btnOK = {
            ...data[3]
          }
        default:
          break;
      }
    }
    return realData;
  }

  show() {
    let wrapper = this.geneRateComfirm(arguments)

    $(this).append(wrapper);
    $('.mc-alert-wrapper').addClass('dp-flex');
    $('.mc-confirm').addClass('swal2-show');
  }

}
window.customElements.define('mc-confirm', Confirm);
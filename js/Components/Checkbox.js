class Checkbox extends HTMLInputElement {
  constructor() {
    super()
    this.labelMask = $(`<label for="" class="check-box cbobox-mr"></label>`);
    this.type = 'checkbox';
    this.checked;
    this.isValid = $(this).is(":checked") === $(this).is(":required");
  }

  getDataFromAttr(attrName) {
    return $(this).attr(attrName) || '';
  }

  connectedCallback() {
    this.setData();
    this.render();
  }

  setData() {
    this.checked = this.getDataFromAttr('checked');
    this.type = this.getDataFromAttr('type');
    this.id = this.getDataFromAttr('id');
  }

  getData(name) {
    return name === 'value' ? this.checked : this[name];
  }

  onClick() {
    let value = !$(this).is(":checked")

    if ($(this).is(":required")) {
      this.isValid = value;
      $(this).data("isValid", value)
    } else {
      $(this).data("isValid", true)
    }
  }

  //Khac voi onClick o cho no change truoc va gia tri  ischecked da thay doi truoc khi vao ham`
  onchange() {
    let value = $(this).is(":checked");

    if ($(this).is(":required")) {
      this.isValid = value;
      $(this).data("isValid", value)
    } else {
      $(this).data("isValid", true)
    }
  }

  render() {
    $(this).empty()
    let {
      id,
      labelMask,
      isValid
    } = this;

    labelMask.attr('for', id).off("click").click((e) => {
      this.onClick(e)
    })

    $(this).after(labelMask)
    $(this).is(":required") ? $(this).data("isValid", isValid) : $(this).data("isValid", true)
  }

  static get observedAttributes() {
    return ["value", "checked", "isValid"]
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue != newValue) {
      switch (name) {
        case 'checked':
          this.render();
          break;

        case 'id':
          $(this).attr('id', newValue);
          break;

        default:
          break;
      }
    }
  }
}

customElements.define("mc-checkbox", Checkbox, {
  extends: "input"
});
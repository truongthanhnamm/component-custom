class listview extends HTMLElement {

  constructor() {
    super();
    this.type;
    this.column;
    this.gridLength;
    this.gridScroll;
    this.paging = false;
    this.data;
    this.showChangeType;
  }

  getDataFromAttr(attrName) {
    return $(this).attr(attrName) || '';
  }

  connectedCallback() {
    this.showChangeType = this.getDataFromAttr('showChangeType') == 'true';
    this.gridScroll = this.getDataFromAttr('showChangeTygridScrollpe') == 'true';
    this.gridLength = parseInt(this.getDataFromAttr('gridLength'), 10) || 3;
    this.data = this.getDataFromAttr('data') || [];
    this.paging = this.getDataFromAttr('paging') || false;
    this.type = this.getDataFromAttr('type') || "detail";
    this.type === 'detail' ? this.column = this.getDataFromAttr('column') || [] : this.gridCount = parseInt(this.getDataFromAttr('gridCount'), 10) || 2;
    this.render();


    // Add event listener
    var id = this.getAttribute("id");
    document.addEventListener("paging-id="+id,function(e){
        console.log("paging ne \n page = "+ e.detail);
    });
  }

  render() {
    let {
      data,
      showChangeType,
      paging,
      gridLength,
      gridScroll,
      column,
      type,
    } = this;
    let me = this;

    switch (type) {
      case 'list':
        break;

      case 'grid':
        $(this).children('content').css('grid-template-columns', `repeat(${gridLength}, 1fr)`);
        break;

      default:
        break;
    }
  }
}

// static get observedAttributes() {
//   return ["type", "gridLength", 'gridScroll']
// }

// attributeChangedCallback(name, oldValue, newValue) {
//   console.log('attributeChangedCallback');
//   switch (name) {
//     case 'type':

//       break;


//     default:
//       break;
//   }
// }
// }

window.customElements.define('mc-listview', listview);
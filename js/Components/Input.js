class InputText extends HTMLInputElement {
  constructor() {
    super()
    this.name;
    this.value;
    this.placeholder = this.placeholder || ' '
    this.type = this.type || 'text';
    this.error;
    this.isValid = true;
    this.autocomplete = this.autocomplete || "off"
  }

  connectedCallback() {
    this.setData()
    this.render()
  }

  getDataFromAttr(attrName) {
    return $(this).attr(attrName) || '';
  }

  setData() {
    this.value = this.getDataFromAttr('value');
    this.placeholder = this.getDataFromAttr('placeholder');
    this.type = this.getDataFromAttr('type');
    this.error = this.getDataFromAttr('error');
  }

  onchange(e) {
    console.log('zxc')
    this.isValid = $(this).is(":valid");
    $(this).data("isValid", this.isValid)
  }

  getData(name) {
    return this[name];
  }

  isValid() {
    return this.isValid
  }

  render() {
    console.log('rendert')
    let {
      error,
      isValid
    } = this;

    isValid = $(this).is(':valid')


    if (error.length > 0) {
      let errorElm = $(`<div class='input-error'>${error}</div>`)
      $(this).parent().append(errorElm)
    }
    //Set data return
    $(this).data("isValid", isValid)
    $(this).data("msg", error)

    $(this).change((e) => {
      this.onchange(e)
    })

  }
  static get observedAttributes() {
    return ['value', 'is'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'value':
        this.value = newValue
        break;

      default:
        break;
    }
  }
}

customElements.define("input-text", InputText, {
  extends: "input"
});
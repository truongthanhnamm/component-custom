class Card extends HTMLElement {
	// get soldOut() {
	// 	return this.hasAttribute('soldOut');
	// }
	// set soldOut(value) {
	// 	value ? this.setAttribute('soldOut', '') : this.removeAttribute('soldOut');
	// }
	// get title() {
	// 	return this.getAttribute('title');
	// }
	// set title(value) {
	// 	this.setAttribute('title', value);
	// }
	// get atribute(attr){
	//   return this.hasAttribute('attr')
	// }
	constructor() {
		super();
	}

	connectedCallback() {
		this.render();
		//this.attachEventHandlers();
	}
	// static get observedAttributes() {
	// 	return [ 'visible', 'title' ];
	// }
	// attributeChangedCallback(name, oldValue, newValue) {
	// 	if (name === 'title' && this.shadowRoot) {
	// 		this.shadowRoot.querySelector('.title').textContent = newValue;
	// 	}
	// 	if (name === 'visible' && this.shadowRoot) {
	// 		if (newValue === null) {
	// 			this.shadowRoot.querySelector('.wrapper').classList.remove('visible');
	// 			// this.dispatchEvent(new CustomEvent('close'));
	// 		} else {
	// 			this.shadowRoot.querySelector('.wrapper').classList.add('visible');
	// 			// this.dispatchEvent(new CustomEvent('open'));
	// 		}
	// 	}
	// }
	
	render() {
		const wrapperClass = this.hasAttribute('soldOut') ? 'asset soldOut' : 'asset';
		const img = this.getAttribute('img');
		const name = this.getAttribute('name');
		const description = this.getAttribute('description');
		const container = document.createElement('div');

		container.innerHTML = `
          <style>
            .asset {
              max-width: 25%;
              padding: 1rem;
              display: inline-block;
              background-color: white;
            }
            .asset.soldOut{
              
            }
            .img-asset {
              width: 100%;
            }
            .description {
              font-size: 0.9rem;
              color: darkgray;
            }
          </style>
          <div class='${wrapperClass}'>
            <img class='img-asset' src="${img}" alt="hinh anh">
            <div class='asset-infor'>
                <div class="name">${name}</div>
                <div class="description">${description}</div>
            </div>
          </div>`;
		const shadowRoot = this.attachShadow({
			mode: 'open'
		});
		shadowRoot.appendChild(container);
		// this.remove();
	}
}
window.customElements.define('mc-card', Card);
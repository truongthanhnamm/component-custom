class listviewItem extends HTMLElement {

  constructor() {
    super();
    this.icon;
    this.icondata;
    this.title = '';
    this.content = '';
    this.label;
    this.img;
    this.listItem;
    this.timeout = null
  }

  render() {
    $(this).empty();
    this.img = this.img || "img/avatar-test.jpg";
    this.listItem = $(`
    <div class='block-1'><img src="${this.img}"/></div>
    <div class='block-2'>
      <div class='title-block2'>${this.title}</div>
      <div class='content-block2'>${this.content}</div>
      <div class='sub-content-block2'><i class="fal fa-shipping-timed"></i> 14 ngày</div>
      <div class='label-block2'>${this.generateBlockIcon()}</div>
    </div>
    `)
    $(this).append(this.listItem)
  }

  static get observedAttributes() {
    return ['icondata', 'icon', 'content', 'title']
  }
  generateBlockIcon() {
    let a;
    if (Array.isArray(this.icon)) {
      a = this.icondata.map((item, index) => {
        return `<span class='cl-vng bg-dark'><i class='${this.icon[index]} cl-blue'></i> ${item}</span>`
      })
    }
    return a;
  }
  attributeChangedCallback(name, oldValue, newValue) {
    let me = this;
    if (oldValue !== newValue) {
      switch (name) {
        case 'icondata':
          this.icondata = newValue.split(',')
          break;

        case 'icon':
          this.icon = newValue.split(',')
          break;

        case 'title':
          this.title = newValue
          break;

        case 'content':
          this.content = newValue
          break;

        default:
          break;
      }
      this.timeout !== null && clearTimeout(this.timeout);
      this.timeout = setTimeout(function () {
        me.render()
      }, 1);
    }

  }
}

window.customElements.define('mc-lvitem', listviewItem);
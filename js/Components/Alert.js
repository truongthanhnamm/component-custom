// window.alert.show('success', 'Tieu de success', 'noi dung success')
// window.alert.show('error', 'Tieu de error', 'noi dung error')
// window.alert.show('warning', 'Tieu de warning', 'noi dung warning')
// window.alert.show('info', 'Tieu de info', 'noi dung information')
class Alert extends HTMLElement {
  constructor() {
    super()
    this.actions = $(`<div class='mc-alert-actions'></div>`)
    this.img = $(`<img src='https://www.freeiconspng.com/img/23194 />`)
    this.okButton = $(`<button class='mc-alert-btn'>OK</button>`)
  }
  connectedCallback() {
    this.registryToWindow()
  }

  registryToWindow() {
    window.alert = this
  }

  hide() {
    $('.mc-alert').removeClass('swal2-show').addClass('swal2-hide')

    setTimeout(() => {
      $('.mc-alert-wrapper').remove()
    }, 300)
  }

  show(type = '', header = '', content = '') {
    const wrapper = $(`
        <div class='mc-alert-wrapper'>
            <div class='mc-alert'>
								<div class='mc-alert-header'>

								<div class="swal2-icon swal2-error swal2-animate-error-icon ${type === 'error' ? 'dp-flex':'dp-none'}">
									<span class="swal2-x-mark">
										<span class="swal2-x-mark-line-left"></span>
										<span class="swal2-x-mark-line-right"></span>
									</span>
								</div>
								<div class="swal2-icon swal2-warning swal2-animate-warning-icon ${type === 'warning' ? 'dp-flex':'dp-none'}"></div>
								<div class="swal2-icon swal2-info swal2-animate-info-icon ${type === 'info' ? 'dp-flex':'dp-none'}"></div>
                <div class="swal2-icon swal2-success swal2-animate-success-icon ${type === 'success' ? 'dp-flex':'dp-none'}">
                    <div class="swal2-success-circular-line-left">
                    </div>
                    <span class="swal2-success-line-tip"></span> 
                    <span class="swal2-success-line-long"></span>
                    <div class="swal2-success-ring"></div> 
                    <div class="swal2-success-fix"></div>
                    <div class="swal2-success-circular-line-right"></div>
                </div>
                    
                    <h2 class="mc-alert-title">${header}</h2>
                </div>
                <div class='mc-alert-content'>${content}</div>
                <div class='mc-alert-actions'>
                </div>
            </div>
        </div>`)

    this.okButton.on('click', () => {
      this.hide()
    })
    wrapper.find('.mc-alert-actions').append(this.okButton)

    $(this).append(wrapper)
    //Cho phép ẩn Alert khi click màn hình màu tối
    $('.mc-alert-wrapper').addClass('dp-flex').click((event) => {
      this.hide()
    })
    //Chặn việc ẩn khi click vào phạm vi của Alert nhưng không phải button
    $('.mc-alert').addClass('swal2-show').click(function (event) {
      event.stopPropagation();
    })
  }
}
window.customElements.define('mc-alert', Alert)
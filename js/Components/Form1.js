class FormManagement {
	get Form() {
		let forms = $('mc-form');
		let result = {};
		for (let i = 0; i < forms.length; i++) {
			result[forms[i].getAttribute('name')] = forms[i];
		}
		return result;
	}
}

window.mcForm = window.mcForm || new FormManagement();
(function (old) {
	$.fn.attr = function () {
		if (arguments.length === 0) {
			if (this.length === 0) {
				return null;
			}

			var obj = {};
			$.each(this[0].attributes, function () {
				if (this.specified) {
					obj[this.name] = this.value;
				}
			});
			return obj;
		}

		return old.apply(this, arguments);
	};
})($.fn.attr);

(function (old) {
	$.fn.removeAttr = function () {
		if (!arguments.length) {
			this.each(function () {
				for (var i = this.attributes.length - 1; i >= 0; i--) {
					jQuery(this).removeAttr(this.attributes[i].name);
				}
			});

			return this;
		}

		return old.apply(this, arguments);
	};
})($.fn.removeAttr);


class Form extends HTMLElement {
	constructor() {
		super()
		this.label;
		this.name;
		this.formData = {};
		this.fields = {};
		this.isValid;
		this.dataItem = {
			data: '',
			msg: '',
			isValid: true
		}
	}

	get value() {
		let formData = {}
		let data = {
			data: '',
			msg: '',
			isValid: true
		}
		this.isValid = true;

		for (let name in this.fields) {
			let {
				isValid,
				msg
			} = this.fields[name].data()

			data = {
				...data,
				data: this.isCheckbox(this.fields[name]) ? this.fields[name].is(":checked") : this.fields[name].val(),
				isValid: isValid || false,
				msg: isValid ? '' : msg || 'unset'
			}
			this.formData[name] = data;

			if (this.isValid) {
				this.isValid = this.isValid === isValid;
			}
		}

		formData['formData'] = this.formData;
		formData['isValid'] = this.isValid;

		return formData;
	}

	isCheckbox(elm) {
		return elm.filter(`[type='checkbox']`).length
	}

	set value(_value) {
		for (let name in this.formData) {
			this.formData[name] = {
				...this.formData[name],
				data: _value[name]
			}
		}
		this.binding();
	}

	getDataFromAttr(attrName) {
		return $(this).attr(attrName) || '';
	}

	// setPositon() {
	// 	let formItem = $(this).find('form-item1')

	// 	formItem.each((index, item) => {
	// 		let length = $(item).children().length;
	// 		let isSelect = false;

	// 		$(item).children().each((index1, item1) => {
	// 			if ($(item1).hasClass('cbobox-flex')) {
	// 				isSelect = true;
	// 			}
	// 		})
	// 		//Neu co nhieu item trong 1 hang thi chia doi vi tri
	// 		isSelect ? $(item).addClass(`repeat-${(length/2).toPrecision(1)}`) : $(item).addClass(`repeat-${length}`)
	// 	})
	// }
	// checkExistInside(item1, tag) {
	// 	return $(item1).is(tag) ? $(item1) : $(item1).find(tag).length !== 0 && $(item1).find(tag)
	// }
	initData() {
		this.name = this.getDataFromAttr('name');
		this.label = this.getDataFromAttr('label');
	}


	connectedCallback() {
		this.initData();
		this.render();
	}

	binding() {
		for (let name in this.fields) {
			//truong hop la checkbox

			if (this.fields[name].filter(`[type='checkbox']`).length) {
				$(this.fields[name]).prop('checked', this.formData[name].data).trigger('change');
			} else
				$(this.fields[name]).val(this.formData[name].data).trigger('change');
		}
	}
	filterAttr(attr) {
		let attrDelete = ['label', 'name', 'tag'];
		attr = {
			...attr,
			id: attr.name
		}
		attrDelete.forEach(item => delete attr[item])
		return attr;
	}

	itemRender(item) {
		let itemName = $(item).attr('name');
		let labelName = $(item).attr('label');
		let tagName = $(item).attr('tag');
		let attr = this.filterAttr($(item).attr());

		let inputElm = $(document.createElement(tagName, attr.is || '')).attr(attr);;
		this.fields[itemName] = inputElm;
		// Fill Data
		this.formData[itemName] = {
			...this.dataItem,
			data: $(inputElm).val()
		};
		inputElm.on('change', function (e) {
			this.formData[itemName] = $(e.currentTarget).val();
		}.bind(this));


		//Create label
		if (labelName != undefined) {
			let labelElm = $(`<label for=${itemName}>${labelName}</label>`)
			$(item).append(labelElm)

		}
		$(item).prepend(inputElm)
		$(item).removeAttr()
		attr.class != undefined && $(item).addClass(attr.class)
	}

	render() {
		let blockName = $(`<div class='title-form'>${this.label}</div>`);
		let listElm = $(this).find('form-item')

		listElm.each((index, element) => {
			this.itemRender(element)
		});

		$(this).prepend(blockName);
	}
}
customElements.define("mc-form", Form);
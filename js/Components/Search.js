class InputSearch extends HTMLElement {
  constructor(attr) {
    super()
    attr = attr === undefined ? {} : attr;
    this.suggestion = ['VietNam', 'ThaiLand', 'Campuchia', 'America', 'France', 'Japan', 'Venezela']
    this.placeholder;
    this.value;
    this.length = 7;
    this.searchAPI;
    this.searchConfig;
    this.timeout = null;
    this.data = attr.data || '';
  }
  getDataFromAttr(attrName) {
    return $(this).attr(attrName) || '';
  }
  setDataAttr(attrName, value) {
    var attr = $(this).attr(attrName);
    if (typeof attr !== typeof undefined && attr !== false) {
      $(this).attr(attrName, value)
    }
    $(this).attr(attrName) || '';
  }

  connectedCallback() {
    this.placeholder = this.getDataFromAttr('placeholder');
    this.value = this.getDataFromAttr('value');
    this.length = this.getDataFromAttr('length');
    this.data = this.getDataFromAttr('data');
    this.searchConfig = this.getDataFromAttr('searchConfig');
    this.render()

  }

  autocomplete(inp, arr, display, value) {

    let me = this;
    let currentFocus = -1;
    inp.on("propertychange input", function (e) {
      console.log('propertychange input');
      let a, b, i, val = this.value;
      const input = this;
      closeAllLists();
      if (!val) {
        return false;
      }

      a = document.createElement("DIV");
      a.setAttribute("id", input.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");

      input.parentNode.appendChild(a);

      for (i = 0; i < arr.length; i++) {
        if (arr[i][display].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          b = document.createElement("DIV");
          b.innerHTML = "<strong>" + arr[i][display].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i][display].substr(val.length);
          b.innerHTML += "<input type='hidden' value='" + arr[i][display] + "'>";
          b.addEventListener("click", function (e) {
            input.value = this.getElementsByTagName("input")[0].value;
            me.setDataAttr('value', input.value)
            closeAllLists();
          });
          a.appendChild(b);
        }
      }
    });

    inp.keydown(function (e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        currentFocus++;
        addActive(x);
      } else if (e.keyCode == 38) { //up
        currentFocus--;

        addActive(x);
      } else if (e.keyCode == 13) {

        e.preventDefault();
        if (currentFocus > -1) {
          if (x) x[currentFocus].click();
        }
      }
    });

    function addActive(x) {

      if (!x) return false;

      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);

      x[currentFocus] != undefined && x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }

    function closeAllLists(elmnt) {
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    document.addEventListener("click", function (e) {
      closeAllLists(e.target);
    });
  }
  checkSuggsKeywords(keywords, data) {
    return keywords == encodeURIComponent(data.toLowerCase())
  }
  getSearchConfig(searchConfig) {
    let arr = ["display", "value"];
    arr.forEach(element => {
      searchConfig = searchConfig.replace(element, `"${element}"`)
    });

    return JSON.parse(searchConfig);
  }

  render = () => {
    let me = this;
    $(me).empty();

    let input = $(`<input class='input-search' value='${me.value}' placeholder='${me.placeholder}'/>`);
    $(me).addClass('wraper-search');
    $(me).append(input);
    $(this).attr('data-bind', me.data)

    input.on("propertychange input", function (e) {
      let {
        value
      } = e.target;

      //create funtion
      let fn = new Function("return " + me.searchAPI)()

      //Sau khi khong go trong vong 0.5s thi moi run API
      me.timeout !== null && clearTimeout(me.timeout);
      me.timeout = setTimeout(function () {
          let reusult = fn(value).then((result) => {
            let temp = [{
              name: "USA",
              value: 'value USA'
            }, {
              name: "VietNamese",
              value: 'value VietNamese'
            }, {
              name: "Sweden",
              value: 'value Sweden'
            }, {
              name: "USA1",
              value: 'value USA1'
            }, {
              name: "VietNamese1",
              value: 'value VietNamese1'
            }, {
              name: "Sweden1",
              value: 'value Sweden1'
            }]
            // {display: "aaa", value: "bbb"}
            let keys = me.getSearchConfig(me.searchConfig);

            // let a = temp.map((item, index) => {
            //   Object.keys(item) = [keys.display, keys.value]
            //   return item
            // });
            // const transformed = temp.map(({
            //   id,
            //   name
            // }) => ({
            //   label: id,
            //   value: name
            // }));
            // console.log(a);

            // console.log(JSON.parse(me.searchConfig));
            me.autocomplete(input, temp.splice(0, me.length), keys.display, keys.value);
          })
        },
        500);

    })
  }

  static get observedAttributes() {
    return ['placeholder', 'value', 'searchapi'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (newValue !== oldValue) {
      switch (name) {

        case 'value':
          this.value = newValue
          break;
        case 'searchapi':
          this.searchAPI = newValue
          break;

        case 'placeholder':
          this.placeholder = newValue
          break;

        default:
          break;
      }
      if (name == 'value') {
        // console.log('chang value')
      }
      this.render()

    }
  }
}
customElements.define("input-search", InputSearch);
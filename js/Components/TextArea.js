class TextArea extends HTMLTextAreaElement {
  constructor() {
    super()
    this.value;
    this.elmArea;
    this.error;
    this.isValid = true;
    this.placeholder = this.placeholder || ' '
  }

  connectedCallback() {
    this.setData();
    this.render()
  }

  setData() {
    this.value = this.getDataFromAttr('value');
    this.error = this.getDataFromAttr('error');
  }

  getDataFromAttr(attrName) {
    return $(this).attr(attrName) || '';
  }

  getData(name) {
    return this[name];
  }

  onchange(e) {
    this.isValid = $(this).is(":valid");
    $(this).data("isValid", this.isValid)
  }

  render() {
    let {
      isValid,
      error
    } = this;

    if (error.length > 0) {
      let errorElm = $(`<div class='input-error'>${error}</div>`)
      $(this).next().after(errorElm)
    }

    isValid = $(this).is(':valid')
    $(this).data("isValid", isValid)
    $(this).data("msg", error)


    $(this).off("change").change((e) => {
      this.onchange(e)
    })
  }
  static get observedAttributes() {
    return ['value'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'value':
        this.value = newValue
        break;

      default:
        break;
    }
    console.log('change');
  }
}

customElements.define("input-area", TextArea, {
  extends: "textarea"
});
export default TextArea;
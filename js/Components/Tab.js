/* <mc-tab>
		<item name="Tab 1">
			<p>Chiều 27/3, phiên tòa xét xử ly hôn của vợ chồng vua cà phê Trung Nguyên là ông Đặng Lê Nguyên Vũ và bà Lê
				Hoàng Diệp Thảo đã chính thức có kết luận. Theo đó, TAND TP.HCM đã quyết định công nhận thuận tình ly hôn giữa
				ông Vũ và bà Thảo. Đồng thời, tòa ra phán quyết phân chia số tài sản chung của hai vợ, chồng vua cà phê, với tỷ
				lệ 40:60.</p>
		</item>
		<item name="Tab 2">
			<p>Theo thống kê, tổng giá trị khối tài sản chung của hai vợ chồng ông Vũ và bà Thảo lên tới 7.502 tỷ đồng gồm
				tiền mặt, vàng sở hữu chung và giá trị số cổ phần tại các doanh nghiệp phát triển trong thời kỳ hôn nhân. Số
				này chưa bao gồm các bất động sản mà cả hai đứng tên với giá trị hàng trăm tỷ đồng khác.</p>
		</item>
		<item name="Tab 3">
			<p>Hàng loạt nữ đại gia, doanh nhân của Việt Nam cũng sẽ xếp sau bà Thảo về số tài sản sở hữu bao gồm nữ đại gia
				Đặng Ngọc Lan (vợ bầu Kiên); bà Cao Thị Ngọc Dung, Chủ tịch HĐQT Vàng bạc Đá quý Phú Nhuận; bà Đặng Huỳnh Ức My,
				Chủ tịch HĐQT Đầu tư Thành Thành Công hay bà Nguyễn Thị Như Loan, Chủ tịch kiêm Tổng giám đốc của Quốc Cường Gia
				Lai…</p>
		</item>
	</mc-tab> */
(function (old) {
  $.fn.removeAttr = function () {
    if (!arguments.length) {
      this.each(function () {
        for (var i = this.attributes.length - 1; i >= 0; i--) {
          jQuery(this).removeAttr(this.attributes[i].name);
        }
      });

      return this;
    }

    return old.apply(this, arguments);
  };
})($.fn.removeAttr);
class TabElement extends HTMLElement {
  constructor() {
    super();
    this.wapper = $(`<div class='tab-component'></div>`);
    this.slider = $(`<div class='slider-tabcomponent'></div>`)
    this.listItem = []
    this.MAX = 4;
    this.type;
  }

  connectedCallback() {
    this.type = this.getDataFromAttr('type')
    this.render()
  }

  getDataFromAttr(attrName) {
    return $(this).attr(attrName) || '';
  }

  convertData() {
    let data = {
      icon: '',
      name: '',
      content: ''
    }

    $(this).children().each((index, item) => {
      data = {
        ...data,
        icon: $(item).attr('icon'),
        name: $(item).attr('name'),
        content: $(item).html().replace('data-bind', 'data').trim()

      }

      this.validateAddItem() &&
        this.listItem.push(data)
    });
  }

  validateAddItem() {
    let check = true;
    //Neu so luong item vuot qua MAX thi khong them vao nua
    if (this.listItem.length >= this.MAX) {
      check = false;
      //Them action de change model ben knockout neu can thiet
    }
    return check;
  }

  onTitleClick(e, index) {
    $('.title-item').removeClass('active')
    $(e).toggleClass('active')
    let width = $(e).width()
    $(this.slider).css('transform', `translateX(${index * width}px)`);

    $(this.wapper).find('.content-tabcomponent').css('transform', `translateX(${index * -100}%)`)
  }

  createBlockContent() {
    let blockContent = $(`<div class='content-tabcomponent'></div>`)
    let interval = null;
    let oldOffset = 0;
    let me = this;

    function onMouseMove(e) {
      let indexShowing;
      let itemShowing;
      let length = 0;
      //$(blockContent).css('transform', `translateX(${-e.offsetX}px)`)
      $(me.wapper).find('.title-tabcomponent .title-item').each((index,item)=>{
        if ($(item).hasClass('active')){
          indexShowing = index;
          itemShowing = item
        }
        length = index;
      })

      //toi slide tiep theo
      if (e.offsetX > oldOffset) {
        indexShowing !== 0 && me.onTitleClick($(itemShowing).prev(), indexShowing-1)
      } else {
        indexShowing !== length && me.onTitleClick($(itemShowing).next()[0], indexShowing+1)
      }
      //$(blockContent).css('transform', `translateX(0)`)
      $(blockContent).off('mousemove')
    }

    $(blockContent).on('mousedown', function (e) {
      oldOffset = e.offsetX
      interval = setInterval(function () {
        $(blockContent).on('mousemove', onMouseMove)
      }, 300);
    });

    $('body').on('mouseup', function (e) {
      clearInterval(interval);
      $(blockContent).off('mousemove')
    });

    $(blockContent).on('mouseout', function (e) {
      clearInterval(interval);
      $(blockContent).off('mousemove')
    });
    return blockContent;
  }
  render() {
    let me = this;
    this.convertData()

    switch (this.type) {
      case "1":
        this.listItem.forEach((item, index) => {

          let {
            name,
            content,
            icon
          } = item;

          let input = $(
            `<input class='input-tab-component' id=${'tab' + index} type="radio" name="tabs" ${index === 0
						? 'checked'
						: ''}>`
          );
          let label = $(`<label class='lab-tab-component' for=${'tab' + index}>${name}</label>`);
          let section = $(`<section id=${'content' + index}>${content}</section>`);

          label.prepend($(`<i class='${icon}'></i>`));

          if (this.wapper.children().length != 0) {
            this.wapper.find('section').first().before(input);
            this.wapper.find('section').first().before(label);
          } else {
            this.wapper.append(input);
            this.wapper.append(label);
          }
          this.wapper.append(section);
        });
        this.wapper.addClass('style-1');
        break;

      case "2":
        let blockTitle = $(`<div class='title-tabcomponent'></div>`)
        let blockContent = me.createBlockContent()

        this.listItem.forEach((item, index) => {
          let {
            name,
            content,
            icon
          } = item;
          let title = $(`<div class='title-item ${index === 0 ? 'active': ''}'><i class='${icon}'></i> ${item.name}</div>`).click((e) => {
            me.onTitleClick(e.currentTarget, index)
          })
          $(blockTitle).append(title)

          let contentItem = $(`<div class='content-item'></div>`)
          $(contentItem).addClass(`item-${index}`).css('left', `${index *100}%`)
          $(contentItem).append(content)
          $(blockContent).append(contentItem);
        });

        this.slider.css('width', `${100 / this.listItem.length}%`)
        $(blockTitle).append(this.slider)
        this.wapper.prepend(blockTitle);
        this.wapper.append(blockContent);
      default:
        break;
    }

    $(this).after(this.wapper);
  }

  static get observedAttributes() {
    return ['length'];
  }

  remove() {
    this.listItem = [];
    this.wapper.empty()
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'length') {
      this.remove()
      this.render()
    }
  }

}
window.customElements.define('mc-tab', TabElement);
class Combobox extends HTMLSelectElement {
  constructor(attr) {
    super()
    attr = attr === undefined ? {} : attr;
    this.label = attr.label || '';
    //this.name = attr.name || '';
    this.value;
    this.displayText;
    this.elmDropdown;
    this.elmList;
    this.data = attr.data || '';
  }

  connectedCallback() {
    this.registryToWindow();
    this.setData();
    this.render();
    this.handleClickOutSide();

  }

  setData() {
    this.value = this.value === '' ? this.getDataFromAttr('value') : this.value;
    this.label = this.label === '' ? this.getDataFromAttr('label') : this.label;
    this.data = this.data === '' ? this.getDataFromAttr('data') : this.data;
    this.elmList = $(`<div class="list"><ul class='select ${this.name}'></ul></div>`);
    this.elmDropdown = $(`<div class='dropdown'></div>`)
  }

  registryToWindow() {
    window.mcCombobox = this
  }

  getDataFromAttr(attrName) {
    return $(this).attr(attrName) || '';
  }

  getData(name) {
    return this[name];
  }

  afterRender = (item, b) => {
    let selected = $(this).find('option:selected');
    this.value = selected.data('display-text') || selected.text();
    this.elmDropdown.find('.current').html(selected.data('display-text') || selected.text());

    //Create li
    let li = $(`<li class='option' data-value='${$(item).val()}'> ${$(item).text()}</li>`);
    li.click((e) => {
      this.onChange(e)
    })
    this.elmList.children().append(li);
  }

  // Close when clicking outside
  handleClickOutSide = () => {
    $(document).on('click', function (event) {
      if ($(event.target).closest('.dropdown').length === 0) {
        $('.dropdown').removeClass('open');
        $('.dropdown .option').removeAttr('tabindex');
      }
      event.stopPropagation();
    });
  }

  // Open/close
  onShow(e) {
    let dropdown = e.currentTarget;
    $(dropdown).not($(dropdown)).removeClass('open');
    $(dropdown).toggleClass('open');
    if ($(dropdown).hasClass('open')) {
      $(dropdown).find('.option').attr('tabindex', 0);
      $(dropdown).find('.selected').focus();
    } else {
      $(dropdown).find('.option').removeAttr('tabindex');
      $(dropdown).focus();
    }
  }

  // Option click
  onChange = (e) => {
    let li = e.currentTarget || e;
    $(li).closest('.list').find('.selected').removeClass('selected');
    $(li).addClass('selected');

    let displayText = $(li).data('display-text') || $(li).text();
    $(li).closest('.dropdown').find('.current').text(displayText);
    $(li).closest('.dropdown').prev('select').val($(li).data('value'))

    //Set data
    this.displayText = displayText;
    this.value = $(li).data('value');

    //required and valid
    $(this).data('isValid', this.required ? this.value !== '' : true)

  }

  render() {
    let me = this;
    //disabled event click
    me.disabled && me.elmDropdown.addClass('disable')

    if (!$(this).next().hasClass('dropdown')) {
      let spanCurrent = $(`<span class="current"></span>`)
      //Add event click show dropdown
      me.elmDropdown.append(spanCurrent).append(me.elmList).click((e) => {
        me.onShow(e);
      })

      $(this).after(me.elmDropdown);

      //Change from data to data-bind for Knockout working
      $(this).attr('data-bind', this.data).removeAttr('data')
    }

    $(this).on('change', ((e) => {
      let item = $(this).next().find('.select li').filter(`[data-value='${this.value}']`)
      me.onChange(item[0])
    }));
  }
}

customElements.define("mc-combobox", Combobox, {
  extends: "select"
});
export default Combobox;
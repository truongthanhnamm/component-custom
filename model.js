function print() {
  console.log(window.Model.isGood);
}


window.Model = new(function () {
  var me = this;
  this.countryName = ko.observable();
  this.data = ko.observableArray([]);
  this.countryPopulation = ko.observable();
  this.testValue = ko.observable('12333333333333333333')
  this.availableCountries = ko.observableArray([{
    display: "USA",
    value: 'value USA'
  }, {
    display: "VietNamese",
    value: 'value VietNamese'
  }, {
    display: "Sweden",
    value: 'value Sweden'
  }, {
    display: "USA1",
    value: 'value USA1'
  }, {
    display: "VietNamese1",
    value: 'value VietNamese1'
  }, {
    display: "Sweden1",
    value: 'value Sweden1'
  }]);
  this.selectedCountry = ko.observable()
  this.isTrue = ko.observable(true)
  this.TabContent = ko.observableArray([{
    icon: "fas fa-chess-queen",
    name: 'Tab 1',
    dataContent: `Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1Tab 1TabTab 1Tab 1Tab 1TabTab 1Tab 1Tab 1TabTab 1Tab 1Tab 1TabTab 1Tab 1Tab 1Tab`
  }, {
    icon: "fas fa-chess-king",
    name: 'Tab 2',
    dataContent: `Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2 Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2Tab 2`
  }, {
    icon: "fas fa-chess-bishop",
    name: 'Tab 3',
    dataContent: `Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3Tab 3`
  }]);
  this.value = ko.observable('12321321')
  this.placeholder = ko.observable('placeholder ====')
  this.value.subscribe(function (s) {
    console.log(s);
  });
  this.changeData = function () {
    // me.isTrue(!me.isTrue());
    // me.value('zxccccccccccccccccccccccc')
    me.TabContent(me.TabContent().splice(0, me.TabContent().length - 1));
  }
  this.searchAPI = async function (val) {
    let me = this;
    let keywords = encodeURIComponent(val)
    let url = 'https://suggestqueries.google.com/complete/search?output=chrome&q=' + keywords;

    let a = await $.ajax({
      url: url,
      dataType: 'jsonp',
      type: 'GET',
    });
    return a[1]
  }
  this.addData = function () {
    let item = {
      icon: "fas fa-chess-bishop",
      name: 'Test item',
      dataContent: `12333333333333333333333333333333333333`
    }
    me.TabContent.push(item);
  }
  this.callBack = function () {
    alert.show('success', 'Test callback', 'Callback thành công')
  }

  $(document).ready(function () {

    ko.applyBindings(window.Model);
  });
});